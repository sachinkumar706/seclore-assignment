#---------PrivateSubnet Server-------------------------------------
resource "aws_security_group" "db" {
    name = "vpc_db"
    description = "Allow incoming Private connections."

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [ var.vpc_cidr ]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = [ var.vpc_cidr ]
    }
 
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
   }


    vpc_id = aws_vpc.dc1.id

    tags = {
        Name = "PrivateServerSG"
    }
}
