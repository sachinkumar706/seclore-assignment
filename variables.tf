variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-1"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "172.16.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "172.16.1.0/24"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    default = "172.16.2.0/24"
}


variable "ssh_port" {
  description = "The port the EC2 Instance should listen on for SSH requests."
  default     = 22
}

variable "aws_access_key" {
  description = "The access key of the account"
  default = "******************"
}

variable "aws_secret_key" {
  description = "The secret key of the account"
  default = "********************"
}

variable "instance_type" {
   description = "Instance name"
   default = "t2.micro"
}

variable "amis" {
    description = "AMIs by region"
    default = {
        us-east-1 = "ami-03d315ad33b9d49c4" #
    }
}

