#-----Create a VPC----------
resource "aws_vpc" "dc1" {
    cidr_block = var.vpc_cidr
   ##gives you an internal host name
    enable_dns_hostnames = true

 tags = { Name = "DC1"
 }
}


#-----Create Internet Gateway------
resource "aws_internet_gateway" "dc1_igw" {
    vpc_id = aws_vpc.dc1.id

 tags = { Name = "DC1_IG"
 }
}


/*
  Public Subnet
*/
resource "aws_subnet" "us-east-1a-public" {
    vpc_id = aws_vpc.dc1.id

    cidr_block = var.public_subnet_cidr
    availability_zone = "us-east-1a"
    tags = {    
    Name = "Public Subnet"
 }
}


/*
  Private Subnet
*/
resource "aws_subnet" "us-east-1b-private" {
    vpc_id = aws_vpc.dc1.id

    cidr_block = var.private_subnet_cidr
    availability_zone = "us-east-1b"
tags = {
    Name = "Private Subnet"
 }
}
# ------------------- Routing ----------

resource "aws_route_table" "dc1-public-route" {
 vpc_id = aws_vpc.dc1.id
# route { cidr_block = "0.0.0.0/0" gateway_id = "${aws_internet_gateway.dc1_igw.id}"
# }
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dc1_igw.id
  }
tags = { Name = "dc1-public-route"
 }
}


#resource "aws_default_route_table" "dc1-default-route" {
#default_route_table_id = "${aws_vpc.dc1.default_route_table_id}"
# tags = { Name = "dc1-default-route"
# }
#}


# ------------Subnet Association -----

resource "aws_route_table_association" "publicsubnet" {
 subnet_id = aws_subnet.us-east-1a-public.id
 route_table_id = aws_route_table.dc1-public-route.id
}


resource "aws_route_table_association" "privatesubnet" {
 subnet_id = aws_subnet.us-east-1b-private.id
 route_table_id = aws_vpc.dc1.default_route_table_id
}
