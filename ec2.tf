resource "aws_instance" "web" {
    ami = lookup(var.amis, var.aws_region)
    availability_zone = "us-east-1a"
    instance_type = var.instance_type
    vpc_security_group_ids = [ aws_security_group.web.id ]
    subnet_id = aws_subnet.us-east-1a-public.id
    associate_public_ip_address = true
    source_dest_check = false
    key_name = "devops_task"
}
